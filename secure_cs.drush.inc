<?php

/**
 * @file
 * Command line utility support for Coder_sniffer module.
 */

/**
 * Implements hook_drush_command().
 */
function secure_cs_drush_command() {
  $items = array();

  $items['secure_codesniff'] = array(
    'description' => dt('Executes PHP_CodeSniffer with DrupalSecure Coding Standards on a particular directory or file.'),
    'callback' => 'drush_secure_cs_code_scan',
    'arguments' => array(
      'path' => dt('The path that you wish to scan.'),
    ),
    'aliases' => array('secure_cs'),
    'options' => array(
      'extensions' => array(
        'description' => dt('Comma delimited list of file extensions to scan.  The default is @default', array(
          '@default' => implode(',', _secure_cs_extensions()),
        )),
        'example-value' => dt('php,module'),
      ),
      'mode' => array(
        'description' => dt('Specify the name of the set of file extensions to scan if no extensions are specified. Options are: default, extended.'),
        'example-value' => dt('extended'),
      ),
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * @param string $section
 *   The help section.
 *
 * @return string
 *   The help text for the command.
 */
function secure_cs_drush_help($section) {
  switch ($section) {
    case 'drush:secure_codesniff':{
      return dt('This command will execute PHP_CodeSniffer on a given path, using Drupal Secure coding definitions.');
    }
  }
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_secure_cs_validate($path = '') {
  if (!_secure_cs_is_compatible_phpcs_installed()) {
    $path_to_phpcs = _secure_cs_get_path();
    if ($path_to_phpcs) {
      return drush_set_error('NOT_INSTALLED', dt('PHP_CodeSniffer is not installed.'));
    }
    else {
      return drush_set_error('INCOMPATIBLE', dt('No compatible version of PHP_CodeSniffer was found; you must have 1.3.4 or higher.  Alternatively, you can symbolically link with the following command: sudo ln -sv @path $(pear config-get php_dir)/PHP/CodeSniffer/Standards/DrupalSecure', array(
        '@path' => _secure_cs_get_standard($path_to_phpcs),
      )));
    }
  }
  if (!file_exists(drush_get_context('DRUSH_OLDCWD') . '/' . $path)) {
    return drush_set_error('NO_PATH', dt('Path not found; cannot scan.'));
  }
}

/**
 * Invoke phpcs.
 */
function drush_secure_cs_code_scan($path = '') {
  // Define the extensions; allow overriding.
  $extensions = drush_get_option('extensions');
  if (!$extensions) {
    $extensions = _secure_cs_extensions(drush_get_option('mode'));
  }

  list($result, $output) = _secure_cs_execute_secure_cs_codesniff($path, $extensions);//, $format, $reportfile);
  _secure_cs_print_results($output);

  // Report failure.
  if (!$result) {
    drush_set_error('PHPCS_FAIL', dt('Found !count issues', array('!count' => count($output))));
  }
  // There is a report file; no idea how many errors/warnings there were.
  //elseif ($reportfile) {
  //  drush_log(dt('PHP_CodeSniffer executed.'), 'success');
  //}
  else {
    drush_log(dt('No issues found'), 'success');
  }
}

/**
 * Build phpcs command, execute, and return results.
 */
function _secure_cs_execute_secure_cs_codesniff($path, $extensions, $report = 'emacs', $reportfile = NULL) {
  $phpcs_path = _secure_cs_get_path();
  $command = $phpcs_path;
  $command .= ' --standard=' . _secure_cs_get_standard($phpcs_path);
  $command .= ' --extensions=' . implode(',', $extensions);
  $command .= ' --report=' . $report;
  $command .= ' -s';

  if (!empty($reportfile)) {
    $command .= ' --report-file=' . $reportfile;
  }
  $command .= ' ' . drush_get_context('DRUSH_OLDCWD') . '/' . $path;

  $result = drush_shell_exec($command);
  $output = drush_shell_exec_output();
  if ($report == 'emacs') {
    $output = _secure_cs_process_phpcs_emacs_output($output);
  }
  return array($result, $output);
}

/**
 * Turn phpcs emacs output into structured array.
 */
function _secure_cs_process_phpcs_emacs_output($output) {
  $structured_output = array();
  foreach ($output as $line) {
    if (!empty($line)) {
      $data = explode(':', $line);
      if (count($data) != 4) {
        continue;
      }
      list($severity, $message) = explode('-', $data[3]);
      $row = array(
        'file' => $data[0],
        'line' => $data[1],
        'column' => $data[2],
        'severity' => '',
        'message' => '',
        'type' => '',
      );
      if (preg_match('/^ (\w+) - (.*) \((.*)\)$/', $data[3], $matches) && !empty($matches)) {
        $row['severity'] = $matches[1];
        $row['message'] = $matches[2];
        $code = explode('.', $matches[3]);
        $row['type'] = $code[count($code) - 2];
      }
      else {
        $row['message'] = $data[3];
      }
      $structured_output[] = $row;
    }
  }
  return $structured_output;
}

/**
 * Print phpcs results as a table.
 *
 * @param $data Array from _secure_cs_process_phpcs_emacs_output().
 */
function _secure_cs_print_results($data) {
  $files = array();
  foreach ($data as $line) {
    $files[$line['file']][] = $line;
  }
  $header = array(dt('Message'), dt('Severity'), dt('Line'), dt('Column'), dt('Type'));
  foreach ($files as $file => $lines) {
    drush_print(dt('File: !file', array('!file' => $file)));
    $table = array($header);
    foreach ($lines as $line) {
      $table[] = array($line['message'], $line['severity'], $line['line'], $line['column'], $line['type']);
    }
    drush_print_table($table, TRUE);
  }
}

/**
 * Defines the default file extensions scanned by PHP_CodeSniffer.
 *
 * @param string $mode
 *   (optional) A mode that can be set to 'extended'. Defaults to an empty
 *   string.
 *
 * @return array
 *   An array of file extensions that cover standard Drupal projects.
 */
function _secure_cs_extensions($mode = '') {
  $extensions = array(
    'php',
    'module',
    'inc',
    'install',
    'test',
    'profile',
    'theme',
  );
  if ('extended' == $mode) {
    $extensions[] = 'css';
    $extensions[] = 'js';
    $extensions[] = 'txt';
    $extensions[] = 'info';
  }
  return $extensions;
}

/**
 * Checks to see if a compatible version of PHP_CodeSniffer is installed.
 *
 * @return bool
 *   A Boolean indicating whether or not a compatible version of
 *   PHP_CodeSniffer is installed.
 */
function _secure_cs_is_compatible_phpcs_installed() {
  // Check to see if phpcs is installed.
  $version = _secure_cs_get_phpcs_version('phpcs');

  if ($version) {
    // Check if Drupal standard is installed.
    if (_secure_cs_is_drupal_standard_installed('phpcs')) {
      return TRUE;
    }
    // A fix was applied after version 1.3.3 that allowed external Sniff paths.
    // Version from git? Good luck.
    if ('@package_version@' == $version) {
      return TRUE;
    }
    // Version number comparison.
    if (version_compare($version, '1.3.3', '>')) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Gets the version of PHP_CodeSniffer.
 *
 * @param string $path_to_phpcs
 *   The path to PHP_CodeSniffer.
 *
 * @return string
 *   The version of PHP_CodeSniffer, or FALSE if it cannot be determined.
 */
function _secure_cs_get_phpcs_version($path_to_phpcs) {
  static $phpcs_version = NULL;

  if (is_null($phpcs_version)) {
    $result = drush_shell_exec($path_to_phpcs . ' --version');

    if ($result) {
      $phpcs_version_array = explode(' ', array_pop(drush_shell_exec_output()));
      $phpcs_version = $phpcs_version_array[2];
    }
    else {
      $phpcs_version = FALSE;
    }
  }

  return $phpcs_version;
}

/**
 * Checks to see if the Drupal Secure profile has been installed.
 *
 * @param string $path_to_phpcs
 *   The path to PHP_CodeSniffer.
 *
 * @return bool
 *   TRUE if Drupal standard profile has been installed; otherwsie, FALSE.
 */
function _secure_cs_is_drupal_standard_installed($path_to_phpcs) {
  static $standard_installed = NULL;

  if (is_null($standard_installed)) {
    $standard_installed = FALSE;
    $result = drush_shell_exec($path_to_phpcs . ' -i');
    if ($result) {
      $phpcs_installed = array_pop(drush_shell_exec_output());
      if (strpos($phpcs_installed, 'DrupalSecure')) {
        $standard_installed = TRUE;
      }
    }
  }

  return $standard_installed;
}

/**
 * Gets the path to PHP_CodeSniffer.
 *
 * @return string
 *   The path to PHP_CodeSniffer.
 */
function _secure_cs_get_path() {
  if (_secure_cs_is_compatible_phpcs_installed()) {
    return 'phpcs';
  }
  // This is a potential place to check for libraries.
  return '';
}

/**
 * Gets the full coding standard for PHP_CodeSniffer.
 *
 * @param string $path_to_phpcs
 *   The path to PHP_CodeSniffer.
 *
 * @return string
 *   The proper phpcs standard definition, full path or just the name.
 */
function _secure_cs_get_standard($path_to_phpcs) {
  $standard_name = 'DrupalSecure';
  if (_secure_cs_is_drupal_standard_installed($path_to_phpcs)) {
    return $standard_name;
  }
  return dirname(__FILE__) . '/' . $standard_name;
}
